#Hack Arizona - The Nest Parasites

This is the repo for The Nest Parasites' #HackArizona hackathon project. 

The Nest Parasites consit of:

 - Garrette MacDuffee
 - Xavier McLallen
 - Adam Dimka (DIMKA!)
 - Mikey

We are attempting a project for the Metropia data visualization challenge.
We attempt to take a file of user data (CSV format) and visualize the traffic
flow, obstructions, etc. For fun we have also checked out the Leap Motion device
and are attempting to use that to manipulate said visualization.


